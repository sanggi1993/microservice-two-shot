# Wardrobify

Team:

* Jonah Han - Which microservice? Hats
* Jisun Lee - Which microservice? Shoes

## Design

## Shoes microservice

Create Shoe model with manufacturer, its model name, its color, a URL for a picture,
Integrate it with wardrobe_api using foreignKey and tie it to the class Bin

## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

There will be a hats model that has fabirc, style name, color and URL for picture, and the location of the wordrobe. In order to access the wordrobe location we use locationVO to access it.
