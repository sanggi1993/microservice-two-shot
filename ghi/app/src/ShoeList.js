import React, { useState, useEffect } from 'react';

function ShoeList() {
    const [shoes, setShoes] = useState([]);

    async function loadShoes() {
        const response = await fetch('http://localhost:8080/api/shoes');

        if (response.ok) {
            const data = await response.json(); //make into javascript object
            setShoes(data.shoes); //whole component will rerender with setShoes, reload shoes

        } else {
            console.error(response);
        };
    };

    useEffect(() => {
        loadShoes();
    }, []); //pass in empty array, will only callback 1 time to avoid inifite loop. takes in dependency array

    const handleDelete = async (href) => {
        const response = await fetch(`http://localhost:8080${href}`, { method: 'DELETE' });
        if (response.ok) {
            const result = shoes.filter(shoe => shoe.href != href);
            //add each shoe that passes the filter (returns true)
            //creating a new list with items that were not selected to be deleted
            setShoes(result);
        };
    }

    return (
        <>
        <h1>Shoes</h1>
        <table className="table table-striped">
            <thead>
                <tr>
                    <th>Manufacturer</th>
                    <th>Model name</th>
                    <th>Color</th>
                    <th>Bin name</th>
                    <th>Delete</th>
                </tr>
            </thead>
            <tbody>

            {shoes.map(shoe => {
                return (
                    <tr key={shoe.href}>
                        <td>{ shoe.manufacturer }</td>
                        <td>{ shoe.model_name }</td>
                        <td>{ shoe.color }</td>
                        <td>{ shoe.bin.closet_name }</td>
                        <td>
                            <button onClick={() => handleDelete(shoe.href)}>Delete</button>
                        </td>
                    </tr>
                );
            })}
            </tbody>
        </table>
        </>
    );
}

export default ShoeList
